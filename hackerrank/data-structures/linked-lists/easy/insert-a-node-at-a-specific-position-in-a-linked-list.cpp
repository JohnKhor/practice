/*
    Insert Node at a given position in a linked list 
    head can be NULL 
    First element in the linked list is at position 0
    Node is defined as 
    struct Node
    {
        int data;
        struct Node *next;
    }
*/
Node* InsertNth(Node *head, int data, int position)
{
    // Complete this method only
    // Do not write main function. 
    Node *new_node = new Node;
    new_node->data = data;
    
    Node *current = head;
    Node *prev = NULL;
    
    for (int i = 0; i < position; i++) {
        prev = current;
        current = current->next;
    }
    
    if (prev == NULL) {
        head = new_node;
    } else {
        prev->next = new_node;
    }
    
    new_node->next = current;
    
    return head;
}
