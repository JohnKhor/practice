/*
    Delete Node at a given position in a linked list 
    Node is defined as 
    struct Node
    {
        int data;
        struct Node *next;
    }
*/
Node* Delete(Node *head, int position)
{
    // Complete this method
    Node *current = head;
    Node *prev = NULL;
    
    for (int i = 0; i < position; i++) {
        prev = current;
        current = current->next;
    }
    
    if (prev == NULL) {
        head = head->next;
    } else {
        prev->next = current->next;
    }
    
    delete current;
    
    return head;
}
