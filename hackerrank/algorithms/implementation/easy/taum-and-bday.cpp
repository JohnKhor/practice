#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    int T;
    cin >> T;

    long long numOfB, numOfW, costB, costW, conversionRate;
    while(T--) {
        cin >> numOfB >> numOfW >> costB >> costW >> conversionRate;
        cout << numOfB * min(costB, costW + conversionRate) + numOfW * min(costW, costB + conversionRate) << endl;
    }

    return 0;
}
