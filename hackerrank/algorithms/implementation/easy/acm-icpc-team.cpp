#include <iostream>
#include <vector>

using namespace std;

int maximalTopics(string a, string b, int M) {
    int maximal = 0;
    for (int i = 0; i < M; i++) {
        maximal += ((a[i] - '0') | (b[i] - '0'));
    }
    return maximal;
}

int main() {
    int N, M;
    cin >> N >> M;
    
    vector<string> topic(N);
    for(int i = 0; i < N; i++){
       cin >> topic[i];
    }

    vector<int> count(M + 1);
    for (int i = 0; i < N; i++) {
        for (int j = i; j < N; j++) {
            count[maximalTopics(topic[i], topic[j], M)]++;
        }
    }

    for (int i = M; i >= 0; i--) {
        if (count[i] > 0) {
            cout << i << endl << count[i];
            break;
        }
    }
    
    return 0;
}
