#include <iostream>

using namespace std;

const int N = 4;
char board[N][N];

bool canWin(char batman) {
    int emptyCount, batmanCount;
    char piece;

    // Verticals
    for (int r = 0; r < N; r++) {
        for (int offsetCol = 0; offsetCol <= 1; offsetCol++) {
            emptyCount = 0, batmanCount = 0;
            for (int c = offsetCol; c < N + offsetCol - 1; c++) {
                piece = board[r][c];
                if (piece == batman) {
                    batmanCount++;
                } else if (piece == '.') {
                    emptyCount++;
                }
            }
            if (batmanCount == 2 && emptyCount == 1) {
                return true;
            }
        }
    }

    // Horizontals
    for (int c = 0; c < N; c++) {
        for (int offsetRow = 0; offsetRow <= 1; offsetRow++) {
            emptyCount = 0, batmanCount = 0;
            for (int r = offsetRow; r < N + offsetRow - 1; r++) {
                piece = board[r][c];
                if (piece == batman) {
                    batmanCount++;
                } else if (piece == '.') {
                    emptyCount++;
                }
            }
            if (batmanCount == 2 && emptyCount == 1) {
                return true;
            }
        }
    }

    // Diagonals
    for (int offsetRow = 0; offsetRow <= 1; offsetRow++) {
        for (int offsetCol = 0; offsetCol <= 1; offsetCol++) {
            // X...
            // .X..
            // ..X.
            // ....
            emptyCount = 0, batmanCount = 0;
            for (int r = offsetRow; r < N + offsetRow - 1; r++) {
                piece = board[r][r - offsetRow + offsetCol];
                if (piece == batman) {
                    batmanCount++;
                } else if (piece == '.') {
                    emptyCount++;
                }
            }
            if (batmanCount == 2 && emptyCount == 1) {
                return true;
            }

            // ...X
            // ..X.
            // .X..
            // ....
            emptyCount = 0, batmanCount = 0;
            for (int r = offsetRow; r < N + offsetRow - 1; r++) {
                piece = board[r][N - 1 - (r - offsetRow) - offsetCol];
                if (piece == batman) {
                    batmanCount++;
                } else if (piece == '.') {
                    emptyCount++;
                }
            }
            if (batmanCount == 2 && emptyCount == 1) {
                return true;
            }    
        }
    }

    return false;
}

int main() {
    int T;
    cin >> T;

    char batman;
    int numOfX, numOfO;
    
    while(T--) {
        numOfX = 0, numOfO = 0;
        for (int r = 0; r < N; r++) {
            for (int c = 0; c < N; c++) {
                cin >> board[r][c];
                
                if (board[r][c] == 'o') {
                    numOfO++;
                } else if (board[r][c] == 'x') {
                    numOfX++;
                }
            }
        }

        if (numOfX > numOfO) {
            batman = 'o';
        } else {
            batman = 'x';
        }
        
        if (canWin(batman)) {
            cout << "YES" << endl;
        } else {
            cout << "NO" << endl;
        }
    }

    return 0;
}
