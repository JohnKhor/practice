#include <iostream>
#include <vector>

using namespace std;

struct Query {
    int type, a, b;
};

int main() {
    int N, Q;
    cin >> N >> Q;

    vector<int> arr(N);
    for (int i = 0; i < N; i++) {
        cin >> arr[i];
    }

    Query q;
    int sum;
    for (int i = 0; i < Q; i++) {
        cin >> q.type >> q.a >> q.b;
        if (q.type == 1) {
            arr[q.a] = q.b;
        } else {
            sum = 0;
            for (int idx = q.a; idx <= q.b; idx++) {
                sum += arr[idx];
            }
            cout << sum << endl;
        }
    }

    return 0;
}
