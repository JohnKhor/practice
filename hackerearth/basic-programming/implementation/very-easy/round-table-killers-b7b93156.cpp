#include <iostream>
#include <vector>

using namespace std;

int main() {
    int N, K, X;
    cin >> N >> K >> X;

    vector<int> killed(N);
    int killer = X, remaining = N, total, victim;
    
    while(true) {
        total = killer % K;

        if (remaining - 1 <= total) { // -1 to exclude the killer
            cout << killer;
            break;
        }
    
        killer -= 1; // 0-indexing

        // Victims
        remaining -= total;
        victim = (killer + 1) % N;
        while(total--) {
            while(killed[victim] == 1) {
                victim = (victim + 1) % N;
            }
            killed[victim] = 1;
        }

        // Next killer
        killer = victim; // instead of (victim + 1) % N since total can be zero
        while(killed[killer] == 1) {
            killer = (killer + 1) % N;
        }

        killer += 1; // 1-indexing
    }

    return 0;
}
