#include <cstdio>
#include <vector>

using namespace std;

struct Fighter {
    int strength;
    bool qualify = true;
    int numOfFights = 0;
};

int main() {
    int N, Q;
    scanf("%d %d", &N, &Q);

    vector<Fighter> fighters(N);
    for (int i = 0; i < N; i++) {
        scanf("%d", &fighters[i].strength);
    }

    int remaining = N;
    int a = 0, b;
    while(remaining != 1) { // Terminating condition: left only one fighter
        while(!fighters[a].qualify) {
            a = (a + 1) % N;
        }

        b = (a + 1) % N;
        while(!fighters[b].qualify) {
            b = (b + 1) % N;
        }
        
        if (a > b) {
            a = b;
            continue;
        }

        if (fighters[a].strength < fighters[b].strength) {
            fighters[a].qualify = false;
        } else {
            fighters[b].qualify = false;
        }
        fighters[a].numOfFights++;
        fighters[b].numOfFights++;

        remaining--;

        a = (b + 1) % N;
    }

    int query;
    while(Q--) {
        scanf("%d", &query);
        printf("%d\n", fighters[query - 1].numOfFights);
    }

    return 0;
}
