#include <iostream>
#include <string>

using namespace std;

int main() {
    string S;
    getline(cin, S);

    int len = S.length();
    int count = 0;
    for (int i = 0; i < len; i++) {
        if (S[i] == ' ') {
            count++;
        }
    }
    cout << count;

    return 0;
}