#include <iostream>
#include <vector>

using namespace std;

int main() {
    int T;
    cin >> T;

    int A, N;
    while(T--) {
        cin >> A >> N;
        
        vector<int> obstacles(N);
        for (int i = 0; i < N; i++) {
            cin >> obstacles[i];
        }

        for (int i = 0; i < N; i++) {
            if (obstacles[i] == 1) {
                A += 2; // gain three units ammo and lose one unit of ammo
            } else {
                A -= 1; // lose one unit ammo
            }

            if (i == N - 1) {
                cout << "Yes " << A << endl;
                break;
            } else if (A == 0) {
                cout << "No " << i + 1 << endl;
                break;
            } 
        }
    }

    return 0;
}
