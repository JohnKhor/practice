#include <iostream>
#include <vector>

using namespace std;

int main() {
    int N, Q;
    cin >> N >> Q;

    vector<long long> prefixSum(N + 1); // 1-indexing
    long long el;
    for (int i = 1; i <= N; i++) {
        cin >> el;
        prefixSum[i] = (i == 1) ? el : prefixSum[i - 1] + el;
    }

    int L, R;
    while(Q--) {
        cin >> L >> R;
        cout << (prefixSum[R] - prefixSum[L - 1]) / (R - L + 1) << endl;
    }

    return 0;
}
