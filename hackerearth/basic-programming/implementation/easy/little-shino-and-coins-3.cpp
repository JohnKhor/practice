#include <iostream>
#include <set>

using namespace std;

int main() {
    int K;
    string S;
    cin >> K >> S;

    int numOfPairs = 0;
    int len = S.length();
    set<int> coins;
    set<int>::iterator it;
    for (int i = 0; i <= len - K; i++) {
        for (int j = i; j < len; j++) {
            // Insert distinct coin into the set
            coins.insert(S[j]);

            // Number of distinct coins in set
            if (coins.size() == K) {
                numOfPairs++;
            } else if (coins.size() > K) {
                break;
            }
        }

        coins.clear();
    }
    cout << numOfPairs;

    return 0;
}
