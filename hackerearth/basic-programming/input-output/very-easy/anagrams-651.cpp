#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;

int main() {
    int T;
    cin >> T;

    string a, b;
    int len, count;
    while(T--) {
        cin >> a >> b;
        vector<int> counts(26);

        len = a.length();
        for (int i = 0; i < len; i++) {
            counts[a[i] - 'a']++;
        }

        len = b.length();
        for (int i = 0; i < len; i++) {
            counts[b[i] - 'a']--;
        }

        count = 0;
        for (int i = 0; i < 26; i++) {
            count += abs(counts[i]);
        }
        cout << count << endl;
    }

    return 0;
}
