#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

void sieve(int N) {
    vector<bool> isPrime(N + 1);
    for (int i = 2; i <= N; i++) {
        isPrime[i] = true;
    }

    int limit = (int)sqrt((double)N);
    for (int p = 2; p <= limit; p++) {
        // found a prime number
        if (isPrime[p]) { 
            // mark multiples of this prime number as false
            for (int m = p * 2; m <= N; m += p) {
                isPrime[m] = false;
            }
        }
    }

    for (int p = 2; p <= N; p++) {
        if (isPrime[p]) {
            cout << p << " ";
        }
    }
}

int main() {
    int N;
    cin >> N;
    sieve(N);
    return 0;
}
