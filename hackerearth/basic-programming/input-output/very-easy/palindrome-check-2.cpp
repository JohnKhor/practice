#include <iostream>

using namespace std;

int main() {
    string S;
    cin >> S;

    bool isPalindrome = true;
    int len = S.length();
    for (int i = 0; i < len/2; i++) {
        if (S[i] != S[len - 1 - i]) {
            isPalindrome = false;
        }
    }

    if (isPalindrome) {
        cout << "YES";
    } else {
        cout << "NO";
    }

    return 0;
}
