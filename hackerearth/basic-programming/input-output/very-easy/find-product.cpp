#include <iostream>
#include <cmath>

using namespace std;

int main() {
    long long answer = 1;
    long long MOD = pow(10, 9) + 7;

    int N;
    cin >> N;

    long long el;
    while(N--) {
        cin >> el;
        answer = ((answer % MOD) * (el % MOD)) % MOD;
    }
    cout << answer;

    return 0;
}
