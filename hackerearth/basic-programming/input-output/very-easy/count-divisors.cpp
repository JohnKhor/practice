#include <iostream>

using namespace std;

int main() {
    int L, R, K;
    cin >> L >> R >> K;

    int count = 0;
    for (int i = L; i <= R; i++) {
        count += (i % K == 0);
    }
    cout << count;

    return 0;
}
