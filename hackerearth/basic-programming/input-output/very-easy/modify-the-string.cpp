#include <iostream>

using namespace std;

int main() {
    string S;
    cin >> S;

    int len = S.length();
    for (int i = 0; i < len; i++) {
        if (S[i] >= 'a' && S[i] <= 'z') {
            S[i] = S[i] - 'a' + 'A';
        } else {
            S[i] = S[i] - 'A' + 'a';
        }
    }
    cout << S;

    return 0;
}
