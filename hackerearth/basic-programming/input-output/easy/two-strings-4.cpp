#include <iostream>
#include <unordered_map>

using namespace std;

int main() {
    int T;
    cin >> T;

    string S1, S2;
    int len;
    bool identical;
    unordered_map<char, int> counts;
    unordered_map<char, int>::iterator itr;
    while(T--) {
        cin >> S1 >> S2;
        counts.clear();

        len = S1.length();
        for (int i = 0; i < len; i++) {
            counts[S1[i]]++;
        }

        len = S2.length();
        for (int i = 0; i < len; i++) {
            counts[S2[i]]--;
        }

        identical = true;
        for (itr = counts.begin(); itr != counts.end(); itr++) {
            if (itr->second != 0) {
                identical = false;
                break;
            }
        }

        if (identical) {
            cout << "YES" << endl;
        } else {
            cout << "NO" << endl;
        }
    }

    return 0;
}
