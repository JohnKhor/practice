#include <iostream>
#include <vector>

using namespace std;

char magicLetter(int c, const vector<int>& primes, int numOfPrimes) {
    if (c <= primes[0]) {
        return (char)primes[0];
    } else if (c >= primes[numOfPrimes - 1]) {
        return (char)primes[numOfPrimes - 1];
    } else {
        for (int i = 1; i < numOfPrimes; i++) {
            if (primes[i] >= c) {
                int distToUpper = primes[i] - c;
                int distToLower = c - primes[i - 1];
                if (distToUpper < distToLower) {
                    return (char)primes[i];
                } else {
                    return (char)primes[i - 1];
                }
            }
        }
    }
}

int main() {
    vector<int> primes = {67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113};
    int numOfPrimes = primes.size();

    int T;
    cin >> T;

    int N;
    string S;
    while(T--) {
        cin >> N >> S;
        
        for (int i = 0; i < N; i++) {
            S[i] = magicLetter((int)S[i], primes, numOfPrimes);
        }
        cout << S << endl;
    }

    return 0;
}
