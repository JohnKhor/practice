#include <iostream>
#include <cctype>

using namespace std;

int main() {
    string S;
    cin >> S;

    int K;
    cin >> K;

    int len = S.length();
    for (int i = 0; i < len; i++) {
        if (isupper(S[i])) {
            S[i] = ((S[i] - 'A') + K) % 26 + 'A';
        } else if (islower(S[i])) {
            S[i] = ((S[i] - 'a') + K) % 26 + 'a'; 
        } else if (isdigit(S[i])) {
            S[i] = ((S[i] - '0') + K) % 10 + '0';
        }
    }
    cout << S;

    return 0;
}
